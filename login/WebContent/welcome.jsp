<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<%

%>
<h3> Hello ${name}</h3>
<div>
<h3> Your Sid : ${sessionID}</h3>
</div>
<br><br>
<form action="LogoutServlet" method="post">
<input type="submit" value="Logout" >
</form>
<data>
    <table style="width:100%">
		<tr>
		    <td>Name</td>
		    <td>Description</td>    
		</tr>
        <c:forEach items="${ls}" var="product">
            <tr>                
                <td>${product.name}</td>
                <td>${product.description}</td>
                <td>
                	<a href="EditItem?id=${product.id}">Edit</a>
                	<a href="delete?id=${product.id}">Delete</a>
                </td>
            </tr>
        </c:forEach>
    </table>
</data>
<a href="additem.jsp">Add item</a>
</body>
</html>