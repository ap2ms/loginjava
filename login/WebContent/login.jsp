<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>login</title>
<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<div align="center" class="form">
		<form action="Login" method="post" class="login-form">
		<input type="text" name="user" required="required" placeholder="User name">
		<input type="password" name="password" required="required" placeholder="password">
		<input type="submit" value="LOGIN">
		</form>
		<a href="registration.jsp">Registration</a>
	</div>
</body>
</html>