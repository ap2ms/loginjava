package jdbc;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import jdbc.ConnectionProvider;
import jdbc.Product;
public class GetAll {
	public static List<Product> getAll(){
		List<Product> ls=new LinkedList<>();		
			String sql="select * FROM items";	
			PreparedStatement preparedStatement;
			try {
				preparedStatement = ConnectionProvider.getConnection().prepareStatement(sql);
				ResultSet r = preparedStatement.executeQuery();
				while(r.next()){
					Product p =new Product(r.getInt(1),r.getString(2),r.getString(3),r.getString(4));    
					ls.add(p);
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}								
		return ls;
	}
	public void edit(int id,String name ,String description ) {
		String sql="update items set name=?,description=? where id=?";
		try {
			PreparedStatement ps=ConnectionProvider.getConnection().prepareStatement(sql);
			ps.setString(1, name);
			ps.setString(2, description);
			ps.setInt(3, id);
			ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static List<Product> getNewById(int id){
		List<Product> ls=new LinkedList<>();		
			String sql="select * FROM items where id="+id;	
			PreparedStatement preparedStatement;
			try {
				preparedStatement = ConnectionProvider.getConnection().prepareStatement(sql);
				ResultSet r = preparedStatement.executeQuery();
				while(r.next()){
					Product p =new Product(r.getInt(1),r.getString(2),r.getString(3),r.getString(4));    
					ls.add(p);
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}								
		return ls;
	} 
	public void delete(int id) {
		String sql="delete items where id=?";
		try {
			PreparedStatement ps=ConnectionProvider.getConnection().prepareStatement(sql);			
			ps.setInt(3, id);
			ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
