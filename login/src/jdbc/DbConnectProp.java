package jdbc;

public interface DbConnectProp {
	public String DB_PATH="jdbc:mysql://localhost:3306/test?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
	public String DB_USER="root";
	public String DB_PASSWORD="QazQwe!23456";
}
