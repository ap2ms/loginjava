package jdbc;

public class Product {
	private int id;
	private String name;
	private String description;
	private String full_text;
	
	public Product(int id, String name, String description, String full_text) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.full_text = full_text;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getFull_text() {
		return full_text;
	}
	public void setFull_text(String full_text) {
		this.full_text = full_text;
	}
}
