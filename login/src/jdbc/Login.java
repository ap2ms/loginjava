package jdbc;

import java.io.IOException;
/*import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;*/

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import jdbc.GetAll;
import jdbc.AuthHelper;
/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
    public Login() {
        super();        
    }

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name=request.getParameter("user");
		String password=request.getParameter("password");		
		String sessionID = null;
		if(AuthHelper.isAllowed(name, password)) {
			
			HttpSession oldSession = request.getSession(false);
            if (oldSession != null) {
                oldSession.invalidate();
            }
            //generate a new session
            HttpSession newSession = request.getSession(true);

            //setting session to expiry in 1 min
            newSession.setMaxInactiveInterval(1*60);
			
            Cookie[] cookies = request.getCookies();
            if(cookies != null){
            for(Cookie cookie : cookies){            
            	if(cookie.getName().equals("JSESSIONID")) sessionID = cookie.getValue();
            }
            }
            
			request.setAttribute("name", name);			
			request.setAttribute("sessionID", sessionID);
			request.setAttribute("ls", GetAll.getAll());
			request.getRequestDispatcher("welcome.jsp").forward(request, response);
		}else {
			response.sendRedirect("login.jsp");	
		}		
	}
}
