package jdbc;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import jdbc.ConnectionProvider;
import java.sql.SQLException;
import jdbc.HashHelper;
public class AuthHelper {
	public static boolean isAllowed(String name, String password) {
		if(name==null ||  password==null) {
			return false;	
		}
		try {			
			String dbName=null;
			String dbPassword=null;
			String sql="select * from users where username=? AND password=?";			
			PreparedStatement ps=ConnectionProvider.getConnection().prepareStatement(sql);
			password=HashHelper.get_SHA_256_SecurePassword(password);
			ps.setString(1, name);
			ps.setString(2, password);
			ResultSet rs=ps.executeQuery();	
			while(rs.next()) {
				dbName=rs.getString(2);
				dbPassword=rs.getString("password");
			}
			if(name.equals(dbName)&&password.equals(dbPassword)) {					
				return true;
			}
		}catch (SQLException e) {			
			e.printStackTrace();
		}		
		return false;									
	}	
}
