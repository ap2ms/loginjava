package jdbc;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectionProvider implements DbConnectProp {
	public static Connection getConnection() {
		Connection con = null;
        try {
        	Class.forName("com.mysql.cj.jdbc.Driver");
        	con = DriverManager.getConnection(DB_PATH,DB_USER,DB_PASSWORD);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }        
        return con;
    }	
}
